import subject_fetcher 
from cache_handler import CacheHandler
from multiprocessing import Process, Queue

username = "##INSERT USERNAME##"
password = "##InSERT PASSWORD##"

NUM_OF_PROCESSES = 5

def create_links_queue(links):
	subject_links_queue = Queue()
	for link in links:
		subject_links_queue.put(link)
	return subject_links_queue

def retrieve_links(username, password):
	client = subject_fetcher.create_login_client(username, password)
	links = subject_fetcher.fetch_subjects_list(client)
	print(f"managed to find {len(links)} subjects")
	return links

def create_processes(proc_num, username, password, subject_links_queue, report_queue):
	processes = []
	for i in range(proc_num):
		p = Process(target=subject_fetcher.create_drainer_process, args=(username, password, subject_links_queue, report_queue))
		processes.append(p)
	return processes

def fetch_links(handler):
	links = retrieve_links(username, password)

	subject_links_queue = create_links_queue(links)
	report_queue = Queue()
	processes = create_processes(NUM_OF_PROCESSES ,username, password, subject_links_queue, report_queue)

	process_counter = NUM_OF_PROCESSES
	counter = 0
	while process_counter != 0:
		isDone, google_id = report_queue.get()
		if isDone:
			process_counter -= 1
			print(f"{process_counter} processes are alve")
		else:
			handler.add(google_id)
			print(f"successfully fetched data id: {google_id}")
		counter += 1
		if counter % 100:
			print(f"{subject_links_queue.qsize()} subjects remain in the queue")

	join_all(processes)

def join_all(processes):
	for p in processes:
		p.join()

def main():
	handler = CacheHandler.load()
	try:
		fetch_links(handler)
	except Exception as e:
		handler.dump()
		raise e
if __name__ == "__main__":
	main()