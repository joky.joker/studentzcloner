import requests
import extractor
import re
from url_mapper import URLS


class DrainerClient():
	def __init__(self):
		pass

def create_login_client(username, password):
	session = requests.Session()
	session.headers.update({
		"User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36",
		})
	
	resp = session.post("https://studentz.co.il/wp-login.php", headers = {
		"Referer": "https://studentz.co.il/wp-login.php",
		}, data = {
		"log" : username,
		"pwd" : password,
		"wp-submit" : '%D7%94%D7%AA%D7%97%D7%91%D7%A8',
		})

	if "wp-login.php" in resp.url:
		raise Exception(f"failed to login {resp.text}")
	
	return session

def fetch_subjects_list(client):
	resp = client.get(URLS["SUBJECTS_PAGE"])
	links = extractor.SUBJECTS_URL_REGEX.findall(resp.text)
	return links

def create_drainer_process(username, password, subjects_queue, reporting_queue):
	pass