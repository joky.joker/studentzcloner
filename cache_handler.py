




class CacheHandler():
	def __init__(self, dump_file):
		self.dump_file = dump_file
		self.subject_cache = set()

	@staticmethod
	def load(dump_file=r"drainer_cache.txt"):
		cache = CacheHandler(dump_file)
		with open(cache.dump_file, "w+") as fd: # w+ - read and write, create empty file if doesnt exist
			lines = map(lambda x: x[:-1], fd.readlines()) #remove \n at the end of the line 
			cache.subject_cache.update(lines)
		return cache

	def add(self, google_drive_id):
		self.subject_cache.add(google_drive_id)

	def doesContain(self, google_drive_id):
		return google_drive_id in self.subject_cache

	def dump(self):
		cache = map(lambda x: x + "\n", list(self.subject_cache)) #add back the \n at the end of the lines
		with open(self.dump_file, "w") as fd:
			fd.writelines(cache)


